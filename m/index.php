<?php
//$cdn_domain = "vua365.r.worldssl.net";
$cdn_domain = "hu247.vip";
include_once('includes/cdn_helper.php');
$prefix = "/m";
$mversion = "/v".(isset($_GET["v"])?$_GET["v"]:"456");
if(!strpos( $_SERVER['HTTP_ACCEPT'], 'image/webp' ))
{
}
$logoimg = "/imgs/logo.png?v=1";
$jsversion = 1;
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!-- preload stylesheet resource via declarative markup -->
	<link rel="preload" href="<?=cdn($prefix.'/main.js',$jsversion)?>" as="script" crossorigin=anonymous;>
	<link rel="preload" href="<?=cdn($prefix.'/cocos2d-js-min.js',$jsversion)?>" as="script" crossorigin=anonymous;>
	<link rel="preload" href="<?=cdn($prefix.$mversion.'/src/settings.js',$jsversion)?>" as="script" crossorigin=anonymous;>
	<link rel="preload" href="<?=cdn($prefix.$mversion.'/src/project.js',$jsversion)?>" as="script" crossorigin=anonymous;>
	<link rel="preload" href="<?=cdn($prefix.'/NoSleep.min.js',$jsversion)?>" as="script" crossorigin=anonymous;>
	<link rel="preload" href="//cdn.jsdelivr.net/npm/webpjs@0.0.2/webpjs.min.js" as="script" crossorigin=anonymous;>

	<link rel="dns-prefetch" href="<?=$cdn_domain?>">
	<link rel="dns-prefetch" href="//cdn.onesignal.com">
	<link rel="dns-prefetch" href="//ajax.googleapis.com">
	<link rel="dns-prefetch" href="//cdn.jsdelivr.net">
	<title>Hũ 247 - Cổng game uy tín xanh chín hàng đầu</title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<!-- tạo shortcut trên đt -->
	<link rel="manifest" href="<?=$prefix.'/manifest.json'?>"/>
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?=cdn($prefix.'',0)?>">
	<meta name="theme-color" content="#ffffff"> 
	<link rel="shortcut icon" type="image/png" href="<?=cdn($prefix.$logoimg)?>"/>
	<!-- place this in a head section -->
	<link rel="apple-touch-icon" href="touch-icon-iphone.pngy">
	<link rel="apple-touch-icon" sizes="152x152" href="<?=cdn($prefix.$logoimg)?>">
	<link rel="apple-touch-icon" sizes="180x180" href="<?=cdn($prefix.$logoimg)?>">
	<link rel="apple-touch-icon" sizes="167x167" href="<?=cdn($prefix.$logoimg)?>">
	<!-- place this in a head section -->
	<meta name="apple-mobile-web-app-title" content="vua365.com">
	<link href="<?=cdn($prefix.'/imgs/splash.png')?>" rel="apple-touch-startup-image">

	<!--http://www.html5rocks.com/en/mobile/mobifying/-->
	<meta name="viewport"
				content="width=device-width,user-scalable=no,initial-scale=1, minimum-scale=1,maximum-scale=1,viewport-fit=cover,target-densitydpi=device-dpi"/>

	<!--https://developer.apple.com/library/safari/documentation/AppleApplications/Reference/SafariHTMLRef/Articles/MetaTags.html-->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
	<meta name="format-detection" content="telephone=no">

	<!-- force webkit on 360 -->
	<meta name="renderer" content="webkit"/>
	<meta name="force-rendering" content="webkit"/>
	<!-- force edge on IE -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<meta name="msapplication-tap-highlight" content="no">

	<!-- force full screen on some browser -->
	<meta name="full-screen" content="yes"/>
	<meta name="x5-fullscreen" content="true"/>
	<meta name="360-fullscreen" content="true"/>
	
	<!-- force screen orientation on some browser -->
	<meta name="screen-orientation" content="<%=orientation%>"/>
	<meta name="x5-orientation" content="<%=orientation%>">

	<!--fix fireball/issues/3568 -->
	<!--<meta name="browsermode" content="application">-->
	<meta name="x5-page-mode" content="app">

	<!--<link rel="apple-touch-icon" href=".png" />-->
	<!--<link rel="apple-touch-icon-precomposed" href=".png" />-->

	<link rel="stylesheet" type="text/css" href="<?=cdn($prefix.'/style-mobile.css')?>"/>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="//cdn.jsdelivr.net/npm/webpjs@0.0.2/webpjs.min.js"></script>

	<script type="text/javascript" src="<?=cdn($prefix.'/NoSleep.min.js',$jsversion)?>" defer></script>
	<style type="text/css">
		#splash{background-size: 100% auto;background-image: url("<?=cdn($prefix.'/imgs/splash.png',1)?>")}
	</style>
</head>
<body>

	<!-- Yêu cầu người dùng xoay ngang màn hình -->
	<div id="popup_1" class="full_popup">
		<p>Vui lòng xoay ngang màn hình để có trải nghiệm game tốt nhất!</p>
	</div>
	<div id="clear_cache_but" style="display:block;position: fixed;
		top: 20%;
		right: 6px;
		z-index: 2;
		background: black;
		padding: 5px;
		color: #fff;
		border: 1px solid #c7c7c7;
		font-size: 0.7em;">
		<p>TẢI LẠI GAME</p>
		<script type="text/javascript">
			jQuery(document).ready(()=>{
				$('#clear_cache_but').slideDown("slow");
				setTimeout(()=>{
					$('#clear_cache_but').slideUp("slow");
				}, 4000);
				$('#clear_cache_but').click(()=>{
					window.location.reload(true);
				});
				var noSleep = new NoSleep();
				noSleep.enable();
				window.cdnRoot = "<?=cdn($prefix.$mversion)?>";

				var elem = document.documentElement;
				function openFullscreen() {
				  if (elem.requestFullscreen) {
					elem.requestFullscreen();
				  } else if (elem.mozRequestFullScreen) { /* Firefox */
					elem.mozRequestFullScreen();
				  } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
					elem.webkitRequestFullscreen();
				  } else if (elem.msRequestFullscreen) { /* IE/Edge */
					elem.msRequestFullscreen();
				  }
				}
				openFullscreen();
			});
		</script>
	</div>
	<style type="text/css">
		#splash {
			background: #171717 url("<?=cdn($prefix.'/imgs/splash.png',1)?>") no-repeat center !important;background-size: 100% auto;
		}
		body{background-image: url("<?=cdn($prefix.'/imgs/bg.png')?>");}
		.full_popup{display: none;position: fixed;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		background: #000000cf;
		z-index: 1;}
		#popup_1 p{font-size: 2em;
		margin: 1em;}
		#ios_guild_but{display:none;position: fixed;
		top: 10%;
		right: 6px;
		z-index: 2;
		background: black;
		padding: 5px;
		color: #fff;
		border: 1px solid #c7c7c7;
		font-size: 0.7em;}
		#popup_2{text-align: center;
		background: #000;padding:20px 0;z-index: 3;overflow: scroll;color: #fff;font-size: 1.7em;}
		#loading_cont{max-width: 200px;margin:0 auto;position: relative;height: 100%;}
		@keyframes spinner {to {transform: rotate(360deg);}}
		#loading_img{max-width: 200px;animation: spinner 1s linear infinite;position: absolute;bottom: 40px;left: 0;}
		#splash .progress-bar{display: none;background-image: url("<?=cdn($prefix.'/imgs/loading-assets/bt.png')?>");background-size: 100% 100%;background-color: #fff0;box-shadow: none;}
		#splash .stripes span{background-image: url("<?=cdn($prefix.'/imgs/loading-assets/1.png')?>"), url("<?=cdn($prefix.'/imgs/loading-assets/3.png')?>"), url("<?=cdn($prefix.'/imgs/loading-assets/2.png')?>");background-size: 17px 100%, 17px 100%, 92% 100%;background-position: left top, right top, center center;background-repeat: no-repeat;box-shadow:none;animation:none;transition: none;background-color:#fff0;}
	</style>
	<script type="text/javascript">
		var is_iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
		
		jQuery(document).ready(($)=>{

			if(is_iOS){
				$('#ios_guild_but').slideDown("slow");
				setTimeout(()=>{
					$('#ios_guild_but').slideUp("slow");
				}, 4000);
			}

			$('#ios_guild_but').click(()=>{
				$('#popup_2').toggle();
			});
			$('#popup_2').click(()=>{
				$('#popup_2').hide();
			});
			try{
				screen.orientation.lock("landscape-primary");
			}
			catch(err){
				console.re.log('lock screen orientation failed"');
				$('#clear_cache_but').html("lock screen orientation failed");
			}
			window.onfocus = function() {
				console.log('current orientation ' + window.orientation);
				console.log('window on focus');
			};
		});
/*		var devWidth, devHeight;
		window.addEventListener('load', function() {
		devWidth  = screen.width;
		devHeight = screen.height;
		});
		window.addEventListener('orientationchange', function () {
		if (devWidth < 768 && (window.orientation === 90 || window.orientation == -90)) {
		  document.body.style.width = devWidth + 'px';
		  document.body.style.height = devHeight + 'px';
		  document.body.style.transform = 'rotate(90deg)';
		  document.body.style.transformOrigin = ''+(devHeight/2)+'px '+(devHeight/2)+'px';
		} else {
		  document.body.removeAttribute('style');
		}
		}, true);*/
	</script>
	
	<canvas id="GameCanvas" oncontextmenu="event.preventDefault()" tabindex="0"></canvas>
	<div id="splash">
		<div id="loading_cont">
			<img id="loading_img" src="<?=cdn($prefix.'/imgs/loading.png',1)?>" alt="">
		</div>
		<div class="progress-bar stripes">
			<span style="width: 0%"></span>
		</div>
	</div>

	<script type="text/javascript">
	var setFullWidthMobileInterval;
	function SetWidthCanvasMobile(){
		var $ = jQuery;
		// Mặc định thì canvas trên mobile đã fullscreen rồi
		// Nhwngg trên các màn hình vuông như ipad thì hiển thị lỗi
		// alert("w width: "+$(window).width()+", height: "+$(window).height()+", ratio: "+($(window).width()/$(window).height()));
		if($(window).width()/$(window).height() < 1.4){
			// set full width, height thì theo tỉ lệ * 1.6 với width
			setFullWidthMobileInterval = setInterval(()=>{
				if($('#splash').is(":visible"))
					return;
				clearInterval(setFullWidthMobileInterval);

				$('#GameCanvas').width( $(window).width() );
				$('#GameCanvas').height( $(window).width()/1.6 );

				// alert("w width: "+$('#GameCanvas').width()+", height: "+$('#GameCanvas').height()+", ratio: "+($('#GameCanvas').width()/$('#GameCanvas').height()));
			}, 1000);

		}
	}
	// SetWidthCanvasMobile();   thất bại - thay đổi kích thước GameCanvas làm sai tọa độ các nút bấm
  </script>
<script src="<?=cdn($prefix.$mversion.'/src/settings.js',$jsversion)?>" charset="utf-8" defer></script>
<script src="<?=cdn($prefix.'/main.js',$jsversion)?>" charset="utf-8" defer></script>

<script type="text/javascript">
	var jsversion = '<?=$jsversion?>';
	jQuery(document).ready(()=>{
		(function () {
				// open web debugger console
				if (typeof VConsole !== 'undefined') {
						window.vConsole = new VConsole();
				}

				var splash = document.getElementById('splash');
				splash.style.display = 'block';

				var cocos2d = document.createElement('script');
				cocos2d.async = true;
				cocos2d.src = window._CCSettings.debug ? 'cocos2d-js.js' : '<?=cdn($prefix.'/cocos2d-js-min.js',$jsversion)?>';

				var engineLoaded = function () {
						document.body.removeChild(cocos2d);
						cocos2d.removeEventListener('load', engineLoaded, false);
						window.boot();
				};
				cocos2d.addEventListener('load', engineLoaded, false);
				document.body.appendChild(cocos2d);
		})();
	});
</script>

<!-- tạo shortcut trên đt -->
<script>
// Check that service workers are registered
if ('serviceWorker' in navigator) {
  // Use the window load event to keep the page load performant
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/m/m-service-worker.js?v=1');
  });
}
</script>
</body>
</html>
