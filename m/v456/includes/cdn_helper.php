<?php
function cdn($url = null,$v = null)
{
	$cdn_enabled		= true;
	global $cdn_domain;
	if(!$cdn_domain)
		$cdn_domain = "https://cdn3.sungames.club/topone";
	$cdn_protocol	   = "http";

	if($url == null || 	$url == '')
	{
		return '';
	}
	$url = (string) $url;

	$pattern = '|^http[s]{0,1}://|i';
	if(preg_match($pattern, $url))
	{
		return $url;
	}

	$pattern = '|^//|';
	if(preg_match($pattern, $url))
	{
		return $url;
	}

	$pattern = '|^/|';
	if(!preg_match($pattern, $url))
	{
		$url = '/' . $url;
	}

	if(!$cdn_enabled)
	{
		return $url;
	}
	else
	{
		return '//' . $cdn_domain . $url . ($v != null ? "?v=".$v : "");
//		return $cdn_protocol . '://' . $cdn_domain . $url;
	}
}
