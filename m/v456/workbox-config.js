module.exports = {
  "globDirectory": "/Users/applehouse/Projects/casinoclient/build/web-mobile",
  "globPatterns": [
    "**/*.{js,json,png,ttf,m4a,jpg,mp3,plist,css}"
  ],
  "swDest": "/Users/applehouse/Projects/casinoclient/build/web-mobile/sw.js",
  "swSrc": "m-service-worker.js"
};