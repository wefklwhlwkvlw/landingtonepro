<?php
function cdn($url = null,$v = null, $cdn_enabled = false)
{
	// $cdn_enabled		= true;
	global $cdn_domain;
	if(!$cdn_domain)
		$cdn_domain = "v365.r.worldssl.net";
	$cdn_protocol	   = "https";

	if($url == null || 	$url == '')
	{
		return '';
	}
	$url = (string) $url;

	$pattern = '|^http[s]{0,1}://|i';
	if(preg_match($pattern, $url))
	{
		return $url;
	}

	$pattern = '|^//|';
	if(preg_match($pattern, $url))
	{
		return $url;
	}

	$pattern = '|^/|';
	if(!preg_match($pattern, $url))
	{
		$url = '/' . $url;
	}

	if(!$cdn_enabled)
	{
		return $url;
	}
	else
	{
		return '//' . $cdn_domain . $url . ($v != null ? "?v=".$v : "");
//		return $cdn_protocol . '://' . $cdn_domain . $url;
	}
}
